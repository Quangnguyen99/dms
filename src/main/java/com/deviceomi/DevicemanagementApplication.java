package com.deviceomi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class DevicemanagementApplication {
    public static void main(String[] args) {
        SpringApplication.run(DevicemanagementApplication.class, args);
    }
}
